with p_individu; use p_individu;

package p_main_outils is

    -- nom : Entrer_Entier
	-- type : procédure
	-- type de retour : /
	-- sémantique : Demander un entier à l'utilisateur
	-- pre : /
	-- post : /
	-- exception : /
	procedure Entrer_Entier(entier : OUT Integer; message : IN String);

    -- nom : Afficher_Message
	-- type : procédure
	-- type de retour : /
	-- sémantique : Afficher un message
	-- pre : /
	-- post : /
	-- exception : /
	procedure Afficher_Message (message : IN STRING; afficher_int : boolean := false; int : in integer := 0; skip : boolean := true);

    -- nom : Valider_Valeur
	-- type : fonction
	-- type de retour : boolean
	-- sémantique : vérifier qu'une valeur est entre min et max (utile pour les date)
	-- pre : /
	-- post : /
	-- exception : /
	function Valider_Valeur (val,min,max : IN Integer) return boolean;

 	-- nom : Entrer_Chaine
	-- type : procédure
	-- type de retour : /
	-- sémantique : Entrer une chaine de caractères
	-- pre : /
	-- post : /
	-- exception : /
	procedure Entrer_Chaine (Str : OUT T_STRING; message : IN String);

    -- nom : Entrer_Valeur_Precise
	-- type : procédure
	-- type de retour : /
	-- sémantique : Demander et contrôler une valeur utile pour les jours ou mois ou années
	-- pre : /
	-- post : /
	-- exception : /
	procedure Entrer_Valeur_Precise (valeur : OUT Integer; min,max : IN Integer; message : IN String);

	-- nom : Demander_Sexe
	-- type : procédure
	-- type de retour : /
	-- sémantique : Demander le sexe d'un individu
	-- pre : /
	-- post : /
	-- exception : /
	procedure Demander_Sexe(Sexe : OUT Character);

end;
