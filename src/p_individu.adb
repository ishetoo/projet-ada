
package body p_individu is

	-- nom : Creer_Individu
	-- sémantique : Creer un individu
	-- type de retour :T_INDIVIDU
	-- pre :
	-- post :
	-- exception :
	function Creer_Individu (id : IN Integer; name : IN T_STRING; prenom : IN T_STRING; date : IN T_DATE; genre : IN Character) return T_INDIVIDU is

		individu : T_INDIVIDU;

	begin
		individu.id := id;

		individu.nom := name;

		individu.prenom := prenom;

		individu.date_de_naissance := date;

		individu.sexe := genre;

		return individu;

	end Creer_Individu;

	-- nom : Egal_Individu
	-- sémantique :
	-- type de retour : boolean
	-- pre :
	-- post :
	-- exception :
	function Egal_Individu (indiv : IN T_INDIVIDU; un_autre : IN T_INDIVIDU) return boolean is

	begin
		return (indiv.id = un_autre.id);

	end Egal_individu;

	-- nom : Egal_Id_Individu
	-- sémantique :
	-- type de retour : boolean
	-- pre :
	-- post :
	-- exception :
	function Egal_Id_Individu (indiv : IN T_INDIVIDU; individu_id : IN Integer) return boolean is

	begin
		return (indiv.id = individu_id);

	end Egal_Id_Individu;


	-- nom : Egal_Individu
	-- sémantique :
	-- type de retour : boolean
	-- pre
	-- post :
	function Creer_Nouveau_Id return integer is

	begin
		count_id := count_id + 1;
		return count_id;

	end Creer_Nouveau_Id;

	-- nom : Set_Count_Id
	-- sémantique :
	-- type de retour : /
	-- pre
	-- post :
	procedure Set_Count_Id(id : in integer) is

	begin

		count_id := id;

	end Set_Count_Id;

	-- Méthodes d'accès
	-- Id

	function Get_Id (indiv : IN T_INDIVIDU) return Integer is

	begin
		 return indiv.id;
 	end Get_Id;

        procedure Set_Id (indiv : IN OUT T_INDIVIDU; id : IN Integer) is

	begin
		indiv.id := id;

	end Set_Id;


	function Get_Nom (indiv : IN T_INDIVIDU) return String is
                 un_nom : T_STRING;
	begin
		un_nom := indiv.nom;

		return un_nom.chaine(un_nom.chaine'First..un_nom.longueur);
 	end Get_Nom;

        procedure Set_Nom (indiv : IN OUT T_INDIVIDU; nom : IN T_STRING) is

	begin
		indiv.nom := nom;

	end Set_Nom;

  	function Get_Prenom (indiv : IN T_INDIVIDU) return String is
                un_prenom : T_STRING;
	begin
		un_prenom := indiv.prenom;

		return un_prenom.chaine(un_prenom.chaine'First..un_prenom.longueur);
 	end Get_Prenom;

        procedure Set_Prenom (indiv : IN OUT T_INDIVIDU; prenom : IN T_STRING) is

	begin
		indiv.prenom := prenom;

	end Set_Prenom;


  	function Get_Date (indiv : IN T_INDIVIDU) return T_DATE is

	begin
		 return indiv.date_de_naissance;
 	end Get_Date;

        procedure Set_Date (indiv : IN OUT T_INDIVIDU; date : IN T_DATE) is

	begin
		indiv.date_de_naissance := date;

	end Set_Date;

  	function Get_Genre (indiv : IN T_INDIVIDU) return Character is

	begin
		 return indiv.sexe;
 	end Get_Genre;

        procedure Set_Genre (indiv : IN OUT T_INDIVIDU; genre : IN Character) is

	begin
		indiv.sexe := genre;

	end Set_Genre;
end;
