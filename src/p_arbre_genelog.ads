-- Package p_arbre_genelog V3

with p_arbre_bin_generig;
with p_individu; use p_individu;

	-- Voici les changements de specs dans cette version
	-- Le paramètre espaces est remplacé par l'id pour pouvoir afficher un arbre depuis un noeud donné

-- Ce package suit la programmation défensive
package p_arbre_genelog is

	-- Déclaration de types
	type T_ARBRE_GENELOG is private;

	-- Instanciation de package p_arbre_bin_generig
	package Arbre_Bin_Gen is new p_arbre_bin_generig (T_ELEMENT => T_INDIVIDU,
       							  						T_ID => Integer,
							  							Egal_Elt => Egal_individu,
							  							Egal_id => Egal_Id_individu);
	use Arbre_Bin_Gen;


	-- Déclaration des exceptions
	ELT_ABSENT: exception;
	ELT_PRESENT: exception;
	PARENTS_DEJA_PRESENTS : exception;
	SAISI_INCONNUE : exception; -- Exception ajoutée dans la V2
	ARBRE_VIDE : exception; -- Exception ajoutée dans la V2

	-- nom : Est_Arbre_Vide  // fonction ajoutée dans la V2
	-- type : fonnction
	-- sémantique : voir si l'arbre est vide ou pas
	-- type de retour : boolean
	-- pre : /
	-- post : /
	-- exception : /
	-- test : Est_Arbre_Vide(arbre) => cela retourne vrai si le arbre est vide
	function Est_Arbre_Vide (Arbre : IN T_ARBRE_GENELOG) return boolean;

	-- nom : Creer_Arbre_Genelog
	-- type : procedure
	-- sémantique : créer un arbre généleagique
	-- pre : /
	-- post : /
	-- exception : /
	-- TESTS : Creer_Arbre_Genelog(arbre, racine) => cela cree un arbre avec la racine
	procedure Creer_Arbre_Genelog (Arbre : IN OUT T_ARBRE_GENELOG; Racine : IN T_INDIVIDU);

	-- nom : Inserer_Parent
	-- type : procedure
	-- sémantique : Inserer un parent à un Individu dans l'arbre donné
	-- pre : /
	-- post : /
	-- exception : ELT_ABSENT,SAISI_INCONNUE, PARENTS_DEJA_PRESENTS
	-- TESTS : EX : individu = 18, parent1 = null ou parent2 = null =>
	-- test1 : Inserer_Parent(arbre,18,19) => individu = 18, parent1 = 19 et parent2 = null
	-- test2 : Inserer_Parent(arbre,18,20) => individu = 18, parent1 = 19 et parent2 = 20
	-- test3 : Inserer_Parent(arbre,18,21) => lever exception PARENTS_DEJA_PRESENTS Dans le cas où les 2 parents sont presents
	-- test5 : Inserer_Parent(arbre,17,18) => lever exception ELT_ABSENT
	-- TESTS : EX : individu = 18, parent1 = 19 ou parent2 = null =>
	-- test1 : Inserer_Parent(arbre,18,22) => individu = 18, parent1 = 19 et parent2 = 22
	procedure Inserer_Parent (Arbre : IN T_ARBRE_GENELOG; Indiv : IN Integer; Parent : IN T_INDIVIDU);

	-- nom : Supprimer_Individu
	-- type : procedure
	-- sémantique : Supprimer un individu donné avec ses ancêtres
	-- pre : /
	-- post : /
	-- exception : ELT_ABSENT, ARBRE_VIDE
	-- TEST : EX : si l'arbre est vide => Supprimer_Individu(arbre,18) => lever exception ARBRE_VIDE
	-- TEST : EX : INDIVIDU : 18 => Supprimer_Individu(arbre,18) => afficher_Arbre(arbre)
	-- TEST : EX : INDIVIDU : 17 n'exsite pas dans l'arbre => Supprimer_Individu(arbre,17) => lever exception ELT_ABSENT
	procedure Supprimer_Individu (Arbre : IN OUT T_ARBRE_GENELOG; Indiv : IN Integer);

	-- nom : Calculer_Ancetres
	-- type : fonction
	-- type de retour : entier
	-- sémantique : Calculer le nombre d'ancêtres connus d'un T_INDIVIDU (lui compris)
	-- pre :
	-- post :
	-- exception : ELT_ABSENT, ARBRE_VIDE
	-- TEST : EX : si l'arbre est vide => Calcuer_Ancetres(arbre,17) => lever exception ARBRE_VIDE
	-- TEST : EX : INDIVIDU : 18 dans le cas où il a 2 générations connues => Calcuer_Ancetres(arbre,18) => 2
	-- TEST : EX : INDIVIDU : 17 n'exsite pas dans l'arbre => Calcuer_Ancetres(arbre,17) => lever exception ELT_ABSENT
	function Calculer_Ancetres (Arbre : IN T_ARBRE_GENELOG; Indiv : IN Integer) return Integer;

	-- nom : Obtenir_Ancetres_Generation
	-- type : procedure
	-- sémantique : Obtenir l'ensemble des ancêtres situés à une certaine génération d'un noeud donné
	-- pré : /
	-- post : /
	-- exception : ELT_ABSENT, ARBRE_VIDE
	-- TEST : EX : si l'arbre est vide => Obtenir_Ancetres_Generation(arbre,17) => lever exception ARBRE_VIDE
	-- TEST : EX : INDIVIDU : 18 dans le cas où il a 2 générations connues => Obtenir_Ancetres_Generation(arbre,18) => 4 si tous les parent sont connus
	-- TEST : EX : INDIVIDU : 17 n'exsite pas dans l'arbre => Obtenir_Ancetres_Generation(arbre,17) => lever exception ELT_ABSENT
	procedure Obtenir_Ancetres_Generation (Arbre : IN T_ARBRE_GENELOG; Generation : IN Integer; Indiv : IN Integer);

	-- nom : Obtenir_Individu_0Parent
	-- type : procedure
	-- sémantique : Obtenir l'ensemble des Individus qui n'ont aucun parent connu
	-- pré : /
	-- post : /
	-- exception : ARBRE_VIDE, ELT_ABSENT
	-- TEST : => Obtenir_Individu_0Parent(arbre) => Affiche (comme une liste) les individus (comme une liste) qui n'ont aucun parent connu
	-- TEST : si l'larbre est vide => Obtenir_Individus_0Parent(arbre) => lever exception ARBRE_VIDE
	procedure Obtenir_Individus_0Parent (Arbre : IN T_ARBRE_GENELOG);

	-- nom : Obtenir_Individu_1Parent
	-- type : procedure
	-- sémantique : Obtenir l'ensemble des Individus qui n'ont qu'un parent connu
	-- pré : /
	-- post : /
	-- exception : ARBRE_VIDE
	-- TEST : => Obtenir_Individus_1Parent(arbre) => Affiche (comme une liste) les individus (comme une liste) qui n'ont aucun parent connu
	-- TEST : si l'larbre est vide => Obtenir_Individus_1Parent(arbre) => lever exception ARBRE_VIDE
	procedure Obtenir_Individus_1Parent (Arbre : IN T_ARBRE_GENELOG);

	-- nom : Obtenir_Individu_2Parent
	-- type : procedure
	-- sémantique : Obtenir l'ensemble des Individus qui ont deux parents connus
	-- pré : /
	-- post : /
	-- exception : ARBRE_VIDE
	-- TEST : => Obtenir_Individus_2Parent(arbre) => Affiche (comme une liste) les individus (comme une liste) qui n'ont aucun parent connu
	-- TEST : si l'arbre est vide => Obtenir_Individus_2Parent(arbre) => lever exception ARBRE_VIDE
	procedure Obtenir_Individus_2Parent (Arbre : IN T_ARBRE_GENELOG);

	-- nom : Afficher_Arbre
	-- type : procedure
	-- sémantique : Afficher un arbre à partir d'un noeud donné
	-- pré : /
	-- post : /
	-- exception : ARBRE_VIDE
	-- test : affichage de l'arbre
	procedure Afficher_Arbre (Arbre : IN T_ARBRE_GENELOG; id : IN Integer); -- // paramètre espaces remplacé par id dans la V3

	private
		-- Le type T_ARBRE_GENELOG instancié depuis le type T_ARBRE_BIN de paquetage p_arbre_bin_generig
		type T_ARBRE_GENELOG is new T_ARBRE_BIN;
end;
