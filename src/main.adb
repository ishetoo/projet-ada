with text_io; use text_io;
with p_arbre_genelog; use p_arbre_genelog;
with p_individu; use p_individu;
with p_main_outils; use p_main_outils;

procedure main is

	-- Déclaration de variables
	start : boolean := true; -- pour démarrer le programme
	choix : Integer; -- la choix saisie
	Arbre : T_ARBRE_GENELOG; -- l'arbre généalogique
	id_individu : Integer; -- un id pour l'individu
	generation : Integer;  -- la génération récupérée

	-- nom : Creer_Un_Individu
	-- type : fonction
	-- type de retour : T_INDIVIDU
	-- sémantique : Demander le sexe d'un individu
	-- pre : /
	-- post : /
	-- exception : /
	function Creer_Un_Individu return T_INDIVIDU is

		-- Déclaration de variable nécéssaires pour creer un individu
		nom : T_STRING;  -- le nom de l'individu
		Prenom : T_STRING; -- le prénom de l'individu
		Jour : Integer; -- le jour  --
		Mois : Integer; -- le mois  --- de naissance
		annee : Integer; -- l'année --
		Sexe : Character; -- le sexe

	begin
		-- Afficher les entrés de clavier
		Entrer_Chaine(nom, "Entrer le nom de famille : ");

		Entrer_Chaine(prenom, "Entrer le prénom : ");

		-- Demander le sexe
		Demander_Sexe(Sexe => Sexe);

		-- Demander le jour
		Entrer_Valeur_Precise(jour,1,31,"Entrer le jour de naissance : ");

		-- Demander le mois
		Entrer_Valeur_Precise(mois,1,12,"Entrer le mois de naissance : ");

		-- Demander l'année
		Entrer_Valeur_Precise(annee,1200,3000,"Entrer l'année de naissance : ");

		-- retourner l'individu créé
		return Creer_Individu (Creer_Nouveau_Id, name => nom, prenom => Prenom, Date => (Jour,Mois,Annee), genre => Sexe);

	end Creer_Un_Individu;

	-- nom : Demande_Initialisation_Arbre
	-- type : procédure
	-- type de retour : /
	-- sémantique : Demander l'initialisation de l'arbre
	-- pre : /
	-- post : /
	-- exception : /
	Procedure Demande_Initialisation_Arbre is
		choix : Character;

	begin
		-- Afficher les entrés de clavier
		put("L'arbre est déjà créé. Vous voulez vraiment l'initialiser ? (o/n) : ");
		get(choix);
		skip_line;

		-- si la réponse est oui alors
		if choix = 'o' then

				-- initiliser le count_id
				Set_Count_Id(0);
				-- Demander la création de l'arbre
				put_line("Veuillez entrer la racine de l'arbre que vous voulez creer");

				-- Appeler la procédure Creer_Arbre_Geneolg
				Creer_Arbre_Genelog(Arbre,Creer_Un_Individu);

				-- Afficher le message si l'arbre est créé
				Afficher_Message("Racine créée",false,0,false);

		-- sinon si la réponse est non alors
		elsif choix = 'n' then
			-- Rien
			null;
		-- sinon
		else
			-- Afficher le message
			Afficher_Message("ERREUR : choix non valide",false,0,false);
		end if;

	end Demande_Initialisation_Arbre;

	-- nom : Traiter_Choix
	-- type : procédure
	-- type de retour : /
	-- sémantique : Traiter les choix saisis par le sous-menu
	-- pre : /
	-- post : /
	-- exception : /
	Procedure Traiter_Choix (choix : IN Integer) is

	begin

		-- selon choix(1) faire
		case choix is

			-- quand c'est 1
			when 1 =>

				-- Demander l'initialisation de l'arbre
				Demande_Initialisation_Arbre;

			-- quand c'est 2
			when 2 =>
				-- Demander l'id
				Entrer_Entier(id_individu, "Entrer l'id de l'individu auquel vous voulez ajouter un parent : ");

				skip_line;

				put_line("Entrer le parent : ");

				-- Appeler la procédure Inserer_Parent
				Inserer_Parent(Arbre,id_individu, Creer_Un_Individu);

				-- Afficher le message
				Afficher_Message("Le parent a été inséré",false,0,false);

			-- quand c'est 3
			when 3 =>

				-- Afficher un message warning
				put_line("ATTENTION : vous allez supprimer l'individu et tous ses ancêtres ! ");

				-- Demander l'id de l'individu souhaité supprimer
				Entrer_Entier(id_individu, "Entrer l'id de l'individu que vous voulez supprimer : ");

				-- Appeler la procédure Suuprimer_Individu
				Supprimer_Individu(Arbre,id_individu);

				-- Afficher le message
				Afficher_Message("L'individu a été supprimé avec ses ancêtres !");

			-- quand c'est 4
			when 4 =>

				-- Demander l'id
				Entrer_Entier(id_individu, "Entrer l'id de l'individu que vous voulez conaître le nombre de ses ancêtres : ");

				-- Afficher le nombre d'ancêtres
				Afficher_Message("Le nombre d'ancêtres d'un individu (lui compris) est : ",true, Calculer_Ancetres(Arbre,id_individu));
				new_line;

			-- quand c'est 5
			when 5 =>

				-- Demander l'id
				Entrer_Entier(id_individu, "Entrer l'id de lindividu : ");

				-- Demander la génération
				Entrer_Entier(generation, "Entrer le nombre de génération : ");

				-- Appeler Obtenir_Ancetres_Generation
				Obtenir_Ancetres_Generation(Arbre, generation, id_individu);

			-- quand c'est 6
			when 6 =>
				--
				new_line;
				put_line("-------------------- Les individus qui n'ont aucun parent connu ---------------------");
				Obtenir_Individus_0Parent(Arbre);

			-- qaund c'est 7
			when 7 =>
				new_line;
				put_line("-------------------- Les individus qui n'ont qu'un seul parent connu ---------------------");
				Obtenir_Individus_1Parent(Arbre);

			-- qaund c'est 8
			when 8 =>

				new_line;
				put_line("-------------------- Les individus qui 2 parents connus ---------------------");
				Obtenir_Individus_2Parent(Arbre);

			-- qaund c'est 9
			when 9 =>

				-- Demander l'id
				Entrer_Entier(id_individu, "Entrer 1 pour afficher tout l'arbre ou bien l'id de l'individu : ");

				-- Afficher l'arbre genéalogique
				Afficher_Arbre(Arbre, id_individu);

			-- quand c'est 0
			when 0 =>
				-- Arrêtrer le programme
				put_line("See you soon ;)");
				start := false;
			when others => Afficher_Message("ERREUR : Choix non connu",false,0,false);
		-- fin selon
		end case;

	-- Récupérer les exceptions
	exception
		when DATA_ERROR => Afficher_Message("ERREUR : Erreur de saisi de clavier");
		when ARBRE_VIDE => Afficher_Message("ERREUR : L'arbre est toujours vide");
		when PARENTS_DEJA_PRESENTS => Afficher_Message("ERREUR : Le(s) parent(s) est/sont déjà présent(s)",false,0,false);
		when ELT_ABSENT => Afficher_Message("ERREUR : L'individu avec l'ID saisi n'exise pas !");
		when SAISI_INCONNUE => Afficher_Message("ERREUR : La siasi est inconnue ou vous avez saisi une valeur non positive !");

	end Traiter_Choix;

	-- nom : Menu_Principal
	-- type : procédure
	-- type de retour : /
	-- sémantique :  Afficher les options du menu
	-- pre : /
	-- post : /
	-- exception : /
	procedure Menu_Principal is

	begin

		put_line("---------------------------- Arbre Genéalogique ----------------------------");
		new_line;

		put_line("1 - Creer un arbre généalogique");
		put_line("0 - Quitter le programme");
		new_line;

		-- Demander un entier
		Entrer_Entier(choix, "Entrer votre choix : ");
		skip_line;

		-- traiter le choix  de l'utilisateur
		case choix is

			-- si le choix est 1 alors on demande de créer un arbre à l'utlisateur
			when 1 =>
				put_line("-----------------------------------------");
				-- afficher message
				put_line("Veuillez entrer la racine de l'arbre que vous voulez creer");

				-- appeler créer arbre genelog
				Creer_Arbre_Genelog(Arbre,Creer_Un_Individu);
				-- afficher le message si l'arbre a été créé
				Afficher_Message("Racine créée",false,0,false);

			-- si le choix est 0
			when 0 =>

				-- afficher le message
				put_line("See you soon ;)");

				-- met start à faux pour arrêter le programme
				start := false;
			--
			when others =>
				-- afficher le message
				Afficher_Message("ERREUR : choix non valide",false,0,false);
		end case;
	-- récuperer l'exception DATA_ERROR
	exception
		when DATA_ERROR => Afficher_Message("ERREUR : Erreur de saisi");

	end Menu_Principal;

	-- nom : Menu_Principal
	-- type : procédure
	-- type de retour : /
	-- sémantique : afficher les options du sous menu
	-- pre : /
	-- post : /
	-- exception : /
	procedure Sous_Menu is

	begin

		put_line("############################### Arbre Genéalogique #################################");
		new_line;
		put_line("1 - Initialiser l'arbre");
		put_line("2 - Ajouter un parent à un individu");
		put_line("3 - Supprimer un individu ");
		put_line("4 - Obtenir le nombre des ancêtres d'un individu");
		put_line("5 - Obtenir l'ensemble des ancêtres d'un individu à une certaine génération");
		put_line("6 - Obtenir l'ensemble des individus qui ont aucun parent connu");
		put_line("7 - Obtenir l'ensemble des individus qui n'ont qu'un seul parent connu");
		put_line("8 - Obtenir l'ensemble des individus qui ont deux parents connus");
		put_line("9 - Afficher l'arbre généalogique à partir d'un noeud donné");
		put_line("0 - Quitter le programme");
		new_line;

		Entrer_Entier(choix, "Entrer votre choix : ");

		-- traiter le choix de l'utilisateur
		Traiter_Choix(choix);

	-- récuperer l'exception DATA_ERROR
	exception
		when DATA_ERROR => Afficher_Message("ERREUR : Erreur de saisi");

	end Sous_Menu;
begin
	-- tant que
	loop

		-- Si l'arbre est vide alors
		if Est_Arbre_Vide(Arbre) then

			-- Lancer le menu principal
			Menu_Principal;
		-- sinon
		else
			-- lancer le sous menu
			Sous_Menu;
		-- fin si
		end if;

	-- jusqu'à start = faux
	exit when not start;

	end loop;

end;
