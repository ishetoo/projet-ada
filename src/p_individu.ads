package p_individu is

	-- Déclaration de type
	type T_INDIVIDU is private;

	type T_DATE is record
		j : Integer;
		m : Integer;
		a : Integer;
	end record;

	type T_STRING is record
		chaine : String(1..20);
		longueur : Integer;
	end record;

	-- Pour la création d'un id automatiquement
	count_Id : Integer := 0;

	-- nom : Creer_Individu
	-- sémantique : Creer un individu
	-- type de retour :T_INDIVIDU
	-- pre :
	-- post :
	-- exception :
	function Creer_Individu (id : IN Integer; name : IN T_STRING; prenom : IN T_STRING; date : IN T_DATE; genre : IN Character) return T_INDIVIDU;

	-- nom : Egal_Individu
	-- sémantique :
	-- type de retour : boolean
	-- pre :
	-- post :
	-- exception :
	function Egal_Individu (indiv : IN T_INDIVIDU; un_autre : IN T_INDIVIDU) return boolean;

	-- nom : Egal_Id_Individu
	-- sémantique :
	-- type de retour : boolean
	-- pre :
	-- post :
	-- exception :
	function Egal_Id_Individu (indiv : IN T_INDIVIDU; individu_id : IN Integer) return boolean;

	-- nom : Creer_Nouveau_Id
	-- sémantique :
	-- type de retour : boolean
	-- pre
	-- post :
	function Creer_Nouveau_Id return integer;

	-- nom : Set_Count_Id
	-- sémantique :
	-- type de retour : /
	-- pre
	-- post :
	procedure Set_Count_Id(id : in integer);

	-- Méthodes d'accès

	-- function Get_Individu (id : IN Integer) return T_INDIVIDU;

	-- ID
	function GeT_ID (indiv : IN T_INDIVIDU) return Integer;

	procedure SeT_ID (indiv : IN OUT T_INDIVIDU; id : IN Integer);

	-- Nom
	function Get_Nom (indiv : IN T_INDIVIDU) return String;

	procedure Set_Nom (indiv : IN OUT T_INDIVIDU; nom : IN T_STRING);

	-- Prenom
	function Get_Prenom (indiv : IN T_INDIVIDU) return String;

	procedure Set_Prenom (indiv : IN OUT T_INDIVIDU; prenom : IN T_STRING);

	-- Date de naissance
	function Get_Date (indiv : IN T_INDIVIDU) return T_DATE;

	procedure Set_Date (indiv : IN OUT T_INDIVIDU; date : IN T_DATE);

	-- Sexe
	function Get_Genre (indiv : IN T_INDIVIDU) return Character;

	procedure Set_Genre (indiv : IN OUT T_INDIVIDU; genre : IN Character);

	-- Déclaration de types
	--
	private
		type T_INDIVIDU is record
			id : Integer;
			prenom : T_STRING;
			nom : T_STRING;
			date_de_naissance : T_DATE;
			sexe : Character;
		end record;
end;
