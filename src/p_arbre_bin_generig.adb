-- Le package p_arbre_bin_generig V3

-- Ce package suit la programmation offensive

package body p_arbre_bin_generig is

	-- nom : Est_Vide
	-- type : fonction
	-- type de retour : boolean
	-- sémantique : Voir si l'arbre est vide ou pas
	-- pre : /
	-- post : /
	-- exception : /
	-- TEST : arbre vide => EST_Vide(arbre) = vrai, faux sinon
	function Est_Vide (arbre : IN T_ARBRE_BIN) return boolean is

	begin
		-- retourner vrai si l'arbre est null
		return (arbre = null);

	end Est_Vide;

	-- nom : Init
	-- type : fonction
	-- type de retour : T_ARBRE_BIN
	-- sémantique : Initialiser un arabre binaire
	-- pre : /
	-- post : Est_vide(arbre)
	-- exception : /
	-- TEST : Est_vide(arbre) = vrai
	function Init return T_ARBRE_BIN is

	begin
		-- initialiser l'arbre
		return null;

	end Init;

	-- nom : Ajouter_Racine
	-- type : procedure
	-- sémantique : Créer la racine d'un arbre
	-- pre : Est_Vide(Fa)
	-- post : non Est_vide(Fa) et alors Egal_Elt(Get_Noeud_Elt(Fa), F_ELEMENT)
	-- exception : /
	-- TEST : racine : 0 => Ajouter_Racine(arbre,racine) => L'arbre a une racine dans ce cas là et n'est plus vide
	procedure Ajouter_Racine (Fa : IN OUT T_ARBRE_BIN; F_ELEMENT : IN T_ELEMENT) is

	begin

		-- Creer la racine de l'arbre
		Fa := new T_NOEUD'(F_ELEMENT, null,null);

	end Ajouter_Racine;

	-- nom : Ajouter_Parent1
	-- type : procedure
	-- sémantique : Ajouter le parent1 (noeud/Fd) à un noeud
	-- pre :  non Est_Vide(Fa) et alors Est_Vide(Get_Parent1(Fa))
	-- post :  non Est_Vide(Fa) et non Est_Vide(Get_Parent1(Fa))
	--       	   et alors Egal_Elt(Get_Noeud_Elt(Get_Parent1(Fa)), F_ELEMENT)
	-- exception : /
	-- TEST : Inserer 18 à 20 en tant que parent1 => 20 a un parent1 qui est 18
	procedure Ajouter_Parent1 (Fa : IN OUT T_ARBRE_BIN; F_ELEMENT : IN T_ELEMENT) is

	begin
		-- Ajouter le parent1
		Fa.all.parent1 := new T_NOEUD'(F_ELEMENT, null,null);

	end Ajouter_Parent1;

	-- nom : Ajouter_Parent2
	-- type : procedure
	-- sémantique : Ajouter le parent2 (noeud/Fg) à un noeud
	-- pre :  non Est_Vide(Fa) et alors Est_Vide(Get_Parent2(Fa))
	-- post :  non Est_Vide(Fa) et non Est_Vide(Get_Parent2(Fa))
	--       	   et alors Egal_Elt(Get_Noeud_Elt(Get_Parent2(Fa)), F_ELEMENT)
	-- exception : /
	-- TEST : Inserer 18 à 20 en tant que parent2 => 20 a un parent2 qui est 18
	procedure Ajouter_Parent2 (Fa : IN OUT T_ARBRE_BIN; F_ELEMENT : IN T_ELEMENT) is

	begin

		-- Ajouter le parent2
		Fa.all.parent2 := new T_NOEUD'(F_ELEMENT, null, null);

	end Ajouter_Parent2;

	-- nom : Supprimer_Parent
	-- type : procedure
	-- type de retour : T_ARBRE_BIN
	-- sémantique : supprimer un parent et tous ses sous-noeuds d'un noeud donné
	-- pre : /
	-- post :  Est_Vide(Rechercher(Fa,Fe));
	-- exception : /
	-- TEST : EX : noeud : 18 parent1 : 19 => Supprimer_Parent (noeud,19) => parent1 = null
	procedure Supprimer_Noeud (Fa : IN OUT T_ARBRE_BIN; Fe : IN T_ID) is

	begin

		-- si Fa /= null et alors Egal_Id(Fa.all.Elt, Fe) alors
		if Fa /= null and then Egal_Id(Fa.all.Elt,Fe) then
			-- supprimer le noeud
			Fa := null;

		-- sinon
		else

			-- Si Fa /= null alors
			if Fa /= null then
				Supprimer_Noeud(Fa.all.parent1,Fe);

			-- sinon rien
			else
				null;
			end if;

			-- Si Fa /= null alors
			if Fa /= null then
				Supprimer_Noeud(Fa.all.parent2,Fe);

			-- sinon rien
			else
				null;
			end if;
		end if;


	end Supprimer_Noeud;

	-- nom : Rechercher
	-- type : function
	-- type de retour : T_ARBRE_BIN
	-- sémantique : Rechercher un noeud dans l'arbre
	-- pre : /
	-- post : /
	-- exception : /
	-- TEST : noeud : 18 => Rechercher(arbre,18) => return arbre = 18
	function Rechercher (Fa : IN T_ARBRE_BIN; Fe : IN T_ID) return T_ARBRE_BIN is

	-- Déclaration de variables temporaires pour garder les REFs de pointeurs
		arbre_temp : T_ARBRE_BIN := Fa;
	begin
		-- si Est_Vide(Fa) alors
		if Fa = null then
			-- return null
			return null;
		-- sinon
		else
			-- si le noeud actuel est le même que le Fe alors
			if Egal_Id(Fa.all.Elt,Fe) then
				-- retourner le noeud
				return Fa;
			-- sinon
			else
				-- on cherche côté parent1 (droite) de l'arbre
				arbre_temp := Rechercher(Fa.all.parent1, Fe);

				-- si on trouvé le noeud demandé alors
				if not Est_vide(arbre_temp) then
					-- retourner le noeud demandé
					return arbre_temp;
				-- sinon on cherche côté parent2 (gauche) de l'arbre
				else
					arbre_temp := Rechercher(Fa.all.parent2, Fe);

					return arbre_temp;

				-- fin si
				end if;

			-- fin si
			end if;

		-- fin si
		end if;

	end Rechercher;

	-- nom : Get_Noeud_Elt
	-- type : function
	-- sémantique : Retourner l'élément d'un noeud
	-- type de retour : T_ELEMENT
	-- pre : non Est_Vide(Fa)
	-- post : /
	-- exception : /
	-- TEST : noeud = 18 => Get_Noeud_Elt(noeud) => 18
	function Get_Noeud_Elt (Fa : IN T_ARBRE_BIN) return T_ELEMENT is

	begin
		-- retourner l'élément
		return Fa.all.Elt;

	end Get_Noeud_Elt;

	-- nom : Get_Parent1
	-- type : function
	-- sémantique : Retourner le parent1 (Fils droite) d'un noeud donné
	-- type de retour : T_ARBRE_BIN /// correction de saisi
	-- pre : /
	-- post : /
	-- exception : /
	-- TEST : noeud : 18 , parent1 : 19 => Get_Parent1(noeud) => noeud = 19
        function Get_Parent1 (Fa : IN T_ARBRE_BIN) return T_ARBRE_BIN is

	begin
		-- si non Est_Vide(Fa) et alors non Est_Vide(Fa*.parent1) alors
		if not Est_vide(Fa) and then not Est_vide(Fa.all.parent1) then
			-- retourner le parent1
			return Fa.all.parent1;
		-- sinon retourner null
		else
			return null;
		end if;

        end Get_Parent1;

	-- nom : Get_Parent2
	-- type : function
	-- sémantique : Retourner le parent2 (Fils gauche) d'un noeud donné
	-- type de retour : T_ARBRE_BIN /// correction de saisi
	-- pre : /
	-- post : /
	-- exception : /
	-- TEST : noeud : 18 , parent2 : 19 => Get_Parent2(noeud) => noeud = 19
	function Get_Parent2 (Fa : IN T_ARBRE_BIN) return T_ARBRE_BIN is

	begin
		-- si non Est_Vide(Fa) et alors non Est_Vide(Fa*.parent2) alors
		if not Est_vide(Fa) and then not Est_vide(Fa.all.parent2) then
			-- retourner le parent2
			return Fa.all.parent2;
		-- sinon retourner null
		else
			return null;
		end if;
	end Get_Parent2;

end;
