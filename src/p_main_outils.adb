with text_io; use text_io;
with ada.integer_text_io; use ada.integer_text_io;

package body p_main_outils is

	MAX : Constant := 20; -- le nombre de caractère max qu'une chaine de caracères peut avoir

    -- nom : Entrer_Entier
	-- type : procédure
	-- type de retour : /
	-- sémantique : Demander un entier à l'utilisateur
	-- pre : /
	-- post : /
	-- exception : /
	procedure Entrer_Entier(entier : OUT Integer; message : IN String) is

	begin
		new_line;
		put(message);
		get(entier);

	end Entrer_Entier;

    -- nom : Afficher_Message
	-- type : procédure
	-- type de retour : /
	-- sémantique : Afficher un message
	-- pre : /
	-- post : /
	-- exception : /
	procedure Afficher_Message (message : IN STRING; afficher_int : boolean := false; int : in integer := 0; skip : boolean := true) is

	--
	begin

		-- Afficher le message
		new_line;
		put_line("----------------------------------------");
		put(message);

		-- si afficher_int est vrai alors on affiche la valeur numérique
		if afficher_int then
			put(int, width => 0);
		-- sinon rien
		else
			null;
		end if;
		new_line;
	    put_line("----------------------------------------");

		-- si skip est vrai alors
		if skip then
			-- supprimer le buffer
			skip_line;
			put("Tapez ENTRER pour continuer : ");
		-- sinon ne supprimer pas le buffer
		else
			put("Tapez ENTRER pour continuer : ");
		end if;
		skip_line;

	end Afficher_Message;

    -- nom : Valider_Valeur
	-- type : fonction
	-- type de retour : boolean
	-- sémantique : vérifier qu'une valeur est entre min et max (utile pour les date)
	-- pre : /
	-- post : /
	-- exception : /
	function Valider_Valeur (val,min,max : IN Integer) return boolean is

		-- Déclaration de variables
		valide : boolean;
	begin
		-- si la valeur est entre max et min alors
		if val <= max and val >= min then

			-- valide <- vrai
			valide := true;
		-- sinon
		else
			-- valide <- faux
			valide := false;
		-- fin si
		end if;
		-- retourner valide
		return valide;
	--
	end Valider_Valeur;

	-- nom : Entrer_Chaine
	-- type : procédure
	-- type de retour : /
	-- sémantique : Entrer une chaine de caractères
	-- pre : /
	-- post : /
	-- exception : /
	procedure Entrer_Chaine (Str : OUT T_STRING; message : IN String) is

		-- Déclaration de variable
		validation : boolean := false; -- pour faire valider la saisi

		chaine : String(1..MAX); -- Une variable pour récupérer une chaîne de caractères.
		taille : Integer;

	begin
			-- tant que
			loop
				-- Afficher le message
				put(message);
				get_line(chaine, taille);

				-- si la taille n'est pas zéro alors
				if taille /= 0 then
					-- si la taille < max alors
					if (taille < max) then
						-- affecter la saisi à str
						Str := (chaine, taille);
						-- validation <- vrai pour arrêter la validation
						validation := true;
					-- sinon
					else
						-- afficher le nombre max de caractères autorisé
						Afficher_Message("Le nombre maximum de caractères autorisé est : ",true,Max);
						new_line;
					end if;
				else
					Afficher_Message("Il faut inserer un caractère",false,0,false);
				end if;
			-- jusqu'à validation = vrai
			exit when validation;
			-- fin tant que
			end loop;

	end Entrer_Chaine;

    -- nom : Entrer_Valeur_Precise
	-- type : procédure
	-- type de retour : /
	-- sémantique : Demander et contrôler une valeur utile pour les jours ou mois ou années
	-- pre : /
	-- post : /
	-- exception : /
	procedure Entrer_Valeur_Precise (valeur : OUT Integer; min,max : IN Integer; message : IN String) is

		-- Déclaration de variables
		valeur_valide : boolean := false;
	begin
		-- tant
		loop
			-- un bloque pour contôler les exceptions
			begin

				-- Demander un entier
				Entrer_Entier(valeur,message);
				skip_line;

				-- Vérifier si E est entre min et max
				valeur_valide := Valider_Valeur(valeur,min,max);

				-- Si typeE_Valide n'est pas vrai alors
				if not valeur_valide then

					-- Afficher le message => Le E doit etre entre (min - max) !
					put("Le valeur doit être entre (");
					put(min,width => 0);
					put("-");
					put(max,width => 0);
					put_line(") !");
				-- sinon
				else
					-- rien
					null;
				-- fin si
				end if;

			-- Afficher le message en cas d'exception DATA_ERROR
			exception
				when DATA_ERROR => Afficher_Message("ERREUR : Erreur de saisi de clavier");

			end;
		-- jusqu'à valeur = vraie
		exit when valeur_valide;

		-- fin tant que
		end loop;
	--
	end Entrer_Valeur_Precise;

	-- nom : Demander_Sexe
	-- type : procédure
	-- type de retour : /
	-- sémantique : Demander le sexe d'un individu
	-- pre : /
	-- post : /
	-- exception : /
	procedure Demander_Sexe(Sexe : OUT Character) is

		-- Déclaration de variables
		choix_sexe : Integer;
	begin
		-- tant que
		loop
			-- un bloque pour récupérer les saisis invalides
			begin
				new_line;
				-- Afficher les options
				put_line("-- Entrer le numéro qui correspond à votre choix --");
				put_line("1 - parent 1");
				put_line("2 - parent 2");

				Entrer_Entier(choix_sexe, "Le numéro ? : ");
				skip_line;

				-- IMPORTANT : le sexe doit être soit 1 (Masculin) soit 2 (Féminin)
				-- si le choix_sexe est 1 alors
				if choix_sexe = 1 then
					-- Sexe <- '1'
					Sexe := '1';
				-- sinon
				else
					-- Sexe <- '2'
					sexe := '2';
				end if;

			-- récupérer l'exception DATA_ERROR
			exception
				when DATA_ERROR => Afficher_Message("ERREUR : Erreur de saisi");
			end;

			-- jusqu'à choix_sexe = 1 ou choix_sexe = 2
			exit when choix_sexe = 1 or choix_sexe = 2;
		end loop;

	end Demander_Sexe;

end;