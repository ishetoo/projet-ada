with p_arbre_bin_generig;
with ada.assertions; use ada.assertions;
with text_io; use text_io;

procedure test_arbre_bin is

	-- fonction qui fait compare 2 entiers
	function Egal_Entier (x : Integer; y : Integer) return boolean is
	begin
		-- returner vrai si x = y
		return (x = y);

	end Egal_Entier;

	-- instantier le package  p_arbre_bin_generig
	package p_arbre_bin is new p_arbre_bin_generig (T_ELEMENT => Integer,
							T_ID => Integer, 
							Egal_Id=> Egal_Entier, 
							Egal_Elt => Egal_Entier);
	use p_arbre_bin;
	
	-- déclaration des variables
	arbre : T_ARBRE_BIN;
	noeud : T_ARBRE_BIN;
begin 

	-- Initialisation de l'arbre
	arbre := Init;

	-- l'arbre doit être vide
	assert(Est_vide(arbre) , "ERREUR : INITIALISATION DE L'ARBRE : Est_Vide(arbre) /= null !!!!");

	---------------------------------------------------------------------------------------------------------
	
	-- Ajouter une racine
	Ajouter_Racine(arbre,0);
	
	-- tester Ajouter_Racine 
	-- l'arbre ne doit plus ếtre vide et Rechercher retourne une résultat
	assert(Egal_Entier(Get_Noeud_Elt(arbre), 0), "ERREUR : L'ARBRE EST ENCORE VIDE Ajouter_Racine(arbre,element) NE MARCHE PAS");

	----------------------------------------------------------------------------------------------------------
	
	-- Ajouter un premier parent 
	-- Le parent1 n'est plus null dans ce cas là
	Ajouter_Parent1(arbre,1);

	-- tester si le parent1 a été bien ajouté
	assert(Get_Noeud_Elt(Get_Parent1(arbre)) = 1, "ERREUR : L'insertion d'un parent à un noeud vide n'a pas marché -> Ajouter_Parent(arbre,element)");

	-- Ajouter un deuxième parent 
	-- Le parent1 a été ajouté précedement donc cette fois c'est le parent2 qui ne devrait plus être null
	Ajouter_Parent2(arbre,2);

	-- tester si le parent2 a été bien ajouté
	assert(Get_Noeud_Elt(Get_Parent2(arbre)) = 2, "ERREUR : L'insertion d'un deuxième parent à un noeud avec un seul parent n'a pas marché -> Ajouter_Parent(arbre,element)");

	-- Ajouter un troisième parent 
	-- une érror doit être levé car la précondition n'a pas été respectée PS : Ajouter pragma pour passer le test
	pragma Ajouter_Parent1(arbre,4);
	
	------------------------------------------------------------------------------------------------------------

	-- Rechercher un élément dans l'arbre à partir d'un noeud
	-- Vu que 2 a été ajouté alors rechercher la valeur de 2 doit retourner un arbre comme noeud racine 2
	noeud := Rechercher(arbre,2);

	-- tester si rechercher une valeur dans l'arbre marche comme prévu
	assert(not Est_Vide(noeud) , "ERREUR : Rechercher(arbre,element) ne retourne pas un sous arbre");

	-- Tester si la racine est bien 2
	
	assert(not Est_Vide(noeud) and then Get_Noeud_Elt(noeud) = 2, "ERREUR : Rechercher(arbre,élément n'a pas retourné le bon élément");

	------------------------------------------------------------------------------------------------------------
	
	-- Ajouter un premier parent pour le noeud 1 
	-- Le parent1 n'est plus null dans ce cas là et en même temps le parent2 reste null
	noeud := Rechercher(arbre,1);

	Ajouter_Parent1(noeud,7);

	-- tester si le parent1 pour le noeud 1 a été bien ajouté
	assert(Get_Noeud_Elt(Get_Parent1(noeud)) = 7, "ERREUR : L'insertion d'un parent à un noeud donné après effectuer un rechercher n'a pas marché -> Ajouter_Parent(arbre,element)");

	-------------------------------------------------------------------------------------------------------------
	
	-- supprimer parent1 = 1
	-- le parent1 sera supprimé avec tous ses ancêtres 
	Supprimer_Noeud(arbre,1);

	noeud := Rechercher(arbre,1);

	assert(Est_Vide(noeud) , "ERREUR : Supprimer(arbre,element) ne supprime pas l'élément ou l'élément supprimé n'est pas le bon élément");

	-- tester si ses ancêtres ont bien supprimés 
	noeud := Rechercher(arbre,7);
	
	assert(Est_Vide(noeud), "ERREUR : Les ancêtres d'un noeud donné supprimé ne sont pas supprimés"); 

exception
	when others => put_line("ERREUR INCONNUE : Probablement le corps des programmes n'est pas encore défini");

end;
