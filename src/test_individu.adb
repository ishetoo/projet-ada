with p_individu; use p_individu;
with ada.assertions; use ada.assertions;


procedure test_individu is
	indiv : T_INDIVIDU;

begin 	
	
	indiv := Creer_Individu (Creer_Nouveau_Id,("Toto1111111111111111",4),("Titi1111111111111111",4),(1,3,2000),'M');
	

	-- Tester les méthodes d'accès
	
	-- Tester si Creer_Individu et les GETs retourne les résultats attendus
	assert(Get_Id(indiv) = 1, "ERREUR : l'id crée n'est pas comme prévu");
	
	assert(Get_Nom(indiv) = "Toto", "ERREUR avec Get_Nom");

	assert(Get_Prenom(indiv) = "Titi", "ERREUR avec Get_Prenom");

	assert(Get_Date(indiv).j = 1, "ERREUR avec Get_Date => le jour n'est pas bien insérée");

	assert(Get_Date(indiv).m = 3, "ERREUR avec Get_Date => le mois n'est pas bien insérée");
	
	assert(Get_Date(indiv).a = 2000, "ERREUR avec Get_Date => l'année n'est pas bien insérée");
	
	assert(Get_Genre(indiv) = 'M', "ERREUR avec Get_Genre");
	
		
	-- Tester les SETs
	set_Id(indiv, 10);

	Set_Genre(indiv, 'F');

	Set_Nom(indiv, ("kaka1111111111111111",4));

	Set_Prenom(indiv, ("Tata1111111111111111",4));

	Set_Date(indiv, (5,5,1999));

	Set_Genre(indiv, 'F');
	
	
	-- Voir si les SETS ont bien modifiés les valeurs 
	assert(Get_Id(indiv) = 10, "ERREUR : l'id crée n'est pas comme prévu");
	
	assert(Get_Nom(indiv) = "kaka", "ERREUR avec Get_Nom");

	assert(Get_Prenom(indiv) = "Tata", "ERREUR avec Get_Prenom");

	assert(Get_Date(indiv).j = 5, "ERREUR avec Get_Date => le jour n'est pas bien insérée");

	assert(Get_Date(indiv).m = 5, "ERREUR avec Get_Date => le mois n'est pas bien insérée");
	
	assert(Get_Date(indiv).a = 1999, "ERREUR avec Get_Date => l'année n'est pas bien insérée");
	
	assert(Get_Genre(indiv) = 'F', "ERREUR avec Get_Genre");
	
end;
