with text_io; use text_io;
with ada.Integer_Text_Io; use ada.Integer_Text_Io;

-- Package p_arbre_genelog V2

-- Ce package suit la programmation défensive
package body p_arbre_genelog is

	-- nom : outils
	-- type : paquetage
	-- sémantique : fournir des sous programmes afin d'éviter la répétion du code
	package outils is

		-- nom : Afficher_Date
		-- type : procedure
		-- sémantique : Afficher une date sous le format jj/mm/aaaa
		-- pré : /
		-- post : /
		-- exception : /
		procedure Afficher_Date(Date : IN T_DATE);

		-- nom : Afficher_Noeud
		-- type : procedure
		-- sémantique : Afficher les informations d'un noeud donné
		-- pré : /
		-- post : /
		-- exception : /
		procedure Afficher_Noeud(Fa : IN T_ARBRE_GENELOG);

		-- nom : Afficher_Noeud_Avec_Espaces
		-- type : procedure
		-- sémantique : Afficher les informations d'un noeud donné avec des espaces
		-- pré : /
		-- post : /
		-- exception : /
		procedure Afficher_Noeud_Avec_Espaces (Fa : IN T_ARBRE_GENELOG; espaces : IN Integer);

		-- nom : Afficher_0Parent
		-- type : procedure
		-- sémantique : Afficher les informations d'un noeud donné qui n'a aucun parent
		-- pré : /
		-- post : /
		-- exception : /
		procedure Afficher_0Parent (Fa : IN T_ARBRE_GENELOG);

		-- nom : Afficher_1Parent
		-- type : procedure
		-- sémantique : Afficher les informations d'un noeud donné qui n'a qu'un seul parent
		-- pré : /
		-- post : /
		-- exception : /
		procedure Afficher_1Parent (Fa : IN T_ARBRE_GENELOG);

		-- nom : Afficher_2Parent
		-- type : procedure
		-- sémantique : Afficher les informations d'un noeud donné qui a deux parents
		-- pré : /
		-- post : /
		-- exception : /
		procedure Afficher_2Parent (Fa : IN T_ARBRE_GENELOG);

		-- nom : Chercher_Ensemble_Parent
		-- type : procedure
		-- sémantique : Parcourir l'arbre pour appliquer les procédures précedentes (Afficher_0Parents, Afficher_1Parent, Afficher_2Parent)
		-- pré : /
		-- post : /
		-- exception : /
		procedure Chercher_Ensemble_Parent(Fa : IN T_ARBRE_GENELOG; nombreParentVoulu : IN Integer);

	end outils;

	-- le corps du paquetage outils
	package body outils is

		-- nom : Afficher_Date
		-- type : procedure
		-- sémantique : Afficher une date sous le format jj/mm/aaaa
		-- pré : /
		-- post : /
		-- exception : /
		procedure Afficher_Date(date : IN T_DATE) is

		begin
			-- afficher la date sous format jj/mm/aaaa
			put(date.j, width => 0);
			put("/");
			put(date.m, width => 0);
			put("/");
			put(date.a, width => 0);
			new_line;

		end Afficher_Date;

		-- nom : Afficher_Noeud
		-- type : procedure
		-- sémantique : Afficher les informations d'un noeud donné
		-- pré : /
		-- post : /
		-- exception : /
		procedure Afficher_Noeud (Fa : IN T_ARBRE_GENELOG) is

		begin

			-- Afficher l'id
			put("- ID : ");
			put(Get_Id(Get_Noeud_Elt(Fa)), width => 0);
			new_line;

			-- Afficher le nom
			put("  NOM : ");
			put_line(Get_Nom(Get_Noeud_Elt(Fa)));

			--Afficher le prenom
			put("  PRENOM : ");
			put_line(Get_Prenom(Get_Noeud_Elt(Fa)));
			put("  DATE DE naissance : ");
			Afficher_Date(Get_Date(Get_Noeud_Elt(Fa)));
			put_line("--------------------------------");

		end Afficher_Noeud;

		-- nom : Afficher_Noeud_Avec_Espaces
		-- type : procedure
		-- sémantique : Afficher les informations d'un noeud donné avec des espaces
		-- pré : /
		-- post : /
		-- exception : /
		procedure Afficher_Noeud_Avec_Espaces (Fa : IN T_ARBRE_GENELOG; espaces : IN Integer) is

			-- Calculer et afficher les espaces qu'il faut
			procedure Afficher_Espaces (espace : IN Integer) is

			begin

				-- pour i de 1 à espace faire
				for i in 1..espace loop

					-- écrire("     ");
					put("          ");

				-- fin pour
				end loop;

			end Afficher_Espaces;
		begin

			-- Afficher l'id
			Afficher_Espaces(espaces);
			put(" ID : ");
			put(Get_Id(Get_Noeud_Elt(Fa)), width => 0);
			new_line;

			-- Afficher le nom
			Afficher_Espaces(espaces);
			put(" NOM : ");
			put_line(Get_Nom(Get_Noeud_Elt(Fa)));

			--Afficher le prenom
			Afficher_Espaces(espaces);
			put(" PRENOM : ");
			put_line(Get_Prenom(Get_Noeud_Elt(Fa)));
			--Afficher la date de naissance
			Afficher_Espaces(espaces);
			put(" DATE DE naissance : ");
			outils.Afficher_Date(Get_Date(Get_Noeud_Elt(Fa)));
			new_line;

		end Afficher_Noeud_Avec_Espaces;

		-- nom : Afficher_0Parent
		-- type : procedure
		-- sémantique : Afficher les informations d'un noeud donné qui n'a aucun parent
		-- pré : /
		-- post : /
		-- exception : /
		procedure Afficher_0Parent (Fa : IN T_ARBRE_GENELOG) is

		begin
			-- si Fa /= null et alors (Fa*.parent1 /= null et Fa*.parent2 /= null) alors
			if not Est_Vide(Fa) and then (Est_Vide(Get_Parent1(Fa)) and Est_Vide(Get_Parent2(Fa))) then

				-- afficher le noeud
				outils.Afficher_Noeud(Fa);
			-- sinon rien
			else
		    	   	null;
			end if;

		end Afficher_0Parent;

		-- nom : Afficher_1Parent
		-- type : procedure
		-- sémantique : Afficher les informations d'un noeud donné qui n'a qu'un seul parent
		-- pré : /
		-- post : /
		-- exception : /
		procedure Afficher_1Parent (Fa : IN T_ARBRE_GENELOG) is

		begin
			-- si Fa /= null et alors (Fa*.parent1 =) null (non ou) Fa*.parent2 = null)  // ici on utilise le XOR pour simplifier la condition
			if not Est_Vide(Fa) and then ((Est_Vide(Get_Parent1(Fa)) xor Est_Vide(Get_Parent2(Fa)))) then

				-- Afficher le noeud
				outils.Afficher_Noeud(Fa);
			-- sinon rien
			else
		    	   	null;
			end if;

		end Afficher_1Parent;

		-- nom : Afficher_2Parent
		-- type : procedure
		-- sémantique : Afficher les informations d'un noeud donné qui a deux parents
		-- pré : /
		-- post : /
		-- exception : /
		procedure Afficher_2Parent (Fa : IN T_ARBRE_GENELOG) is

		begin
			-- si Fa /= null et alors (Fa*.parent1 /= null et alors Fa.parent2 /= null) alors
			if not Est_Vide(Fa) and then (not Est_Vide(Get_Parent1(Fa)) and then not Est_Vide(Get_Parent2(Fa))) then

				-- afficher le noeud
				outils.Afficher_Noeud(Fa);
			else
		    	   	null;
			end if;

		end Afficher_2Parent;

		-- nom : Chercher_Ensemble_Parent
		-- type : procedure
		-- sémantique : Parcourir l'arbre pour appliquer les procédures précedentes (Afficher_0Parents, Afficher_1Parent, Afficher_2Parent)
		-- pré : /
		-- post : /
		-- exception : /
		procedure Chercher_Ensemble_Parent (Fa : IN T_ARBRE_GENELOG; nombreParentVoulu : IN Integer) is

		begin

			-- si le nombre de parents voulu est 0 alors
			if (nombreParentVoulu = 0) then
			-- Afficher les noeud qui n'ont aucun parents connus
				Afficher_0Parent(Fa);

			-- sinon si si le nombre de parents voulu est 1 alors
			elsif nombreParentVoulu = 1 then

				-- Afficher les noeuds qui n'ont qu'un seul parent connu
				Afficher_1Parent(Fa);

			-- sinon si si le nombre de parents voulu est 2 alors
			elsif nombreParentVoulu = 2 then

				-- Afficher les noeuds qui ont 2 parents connus
				Afficher_2Parent(Fa);

			-- sinon lever SAISI_INCONNUE
			else
				raise SAISI_INCONNUE;

			-- fin si
			end if;

			-- parcourir la partie droite du noeud
			--
			-- le parent1 (Fd) n'est pas null alors
			if not Est_Vide(Get_Parent1(Fa)) then

				-- appeler la fonction recursive
				Chercher_Ensemble_Parent(Get_Parent1(Fa), nombreParentVoulu);
			-- sinon
			else
				null;
			end if;

			-- parcourir la partie gauche du noeud
			--
			-- si le parent2 (Fg) n'est pas vide alors
			if not Est_Vide(Get_Parent2(Fa)) then
				-- appeler la fonction récursivement
				Chercher_Ensemble_Parent(Get_Parent2(Fa), nombreParentVoulu);
			-- sinon
			else
				null;
			end if;
		end Chercher_Ensemble_Parent;

	end outils;

	-- nom : Est_Arbre_Vide  // fonction ajoutée dans la V2
	-- type : fonnction
	-- sémantique : voir si l'arbre est vide ou pas
	-- type de retour : boolean
	-- pre : /
	-- post : /
	-- exception : /
	-- test : Est_Arbre_Vide(arbre) => cela retourne vrai si le arbre est vide
	function Est_Arbre_Vide (Arbre : IN T_ARBRE_GENELOG) return boolean is

	begin
		-- retourner vrai si l'arbre est vide
		return Est_Vide(Arbre);

	end Est_Arbre_Vide;

	-- nom : Creer_Arbre_Genelog
	-- type : procedure
	-- sémantique : créer un arbre généleagique
	-- pre : /
	-- post : /
	-- exception : /
	-- TESTS : Creer_Arbre_Genelog(arbre, racine) => cela cree un arbre avec la racine
	procedure Creer_Arbre_Genelog (Arbre : IN OUT T_ARBRE_GENELOG; Racine : IN T_INDIVIDU) is

	begin

		-- Initialiser l'arbre
		Arbre := Init;

		-- Inserer la racine
		Ajouter_Racine(Arbre, Racine);

	end Creer_Arbre_Genelog;


	-- nom : Inserer_Parent
	-- type : procedure
	-- sémantique : Inserer un parent à un Individu dans l'arbre donné
	-- pre : /
	-- post : /
	-- exception : ELT_ABSENT,SAISI_INCONNUE, PARENTS_DEJA_PRESENTS
	-- TESTS : EX : individu = 18, parent1 = null ou parent2 = null =>
	-- test1 : Inserer_Parent(arbre,18,19) => individu = 18, parent1 = 19 et parent2 = null
	-- test2 : Inserer_Parent(arbre,18,20) => individu = 18, parent1 = 19 et parent2 = 20
	-- test3 : Inserer_Parent(arbre,18,21) => lever exception PARENTS_DEJA_PRESENTS Dans le cas où les 2 parents sont presents
	-- test5 : Inserer_Parent(arbre,17,18) => lever exception ELT_ABSENT
	-- TESTS : EX : individu = 18, parent1 = 19 ou parent2 = null =>
	-- test1 : Inserer_Parent(arbre,18,22) => individu = 18, parent1 = 19 et parent2 = 22
	procedure Inserer_Parent (Arbre : IN T_ARBRE_GENELOG; Indiv : IN Integer; Parent : IN T_INDIVIDU) is

		-- Déclaration de variables
		noeud : T_ARBRE_GENELOG;
	begin

		-- Si l'arbre n'est pas vide
		if not Est_Vide(Arbre) then

			-- Rechercher l'individu
			noeud := Rechercher(Arbre,Indiv);

			-- si l'individu est trouvé
			if not Est_Vide(noeud) then

				-- si le parent1 est vide et alors le genre est (1) masculin ou père
				if Est_Vide(Get_Parent1(noeud)) and then Get_Genre(Parent) = '1' then

					-- Ajouter le parent1
					Ajouter_Parent1(Noeud,Parent);

				-- sinon si le parent2 est vide et alors le genre est (2) Féminin ou mère
				elsif Est_Vide(Get_Parent2(noeud)) and then Get_Genre(Parent) = '2' then

					-- Ajouter le parent1
					Ajouter_Parent2(Noeud,Parent);

				-- sinon lever PARENTS_DEJA_PRESENT
				else
					raise PARENTS_DEJA_PRESENTS;
				-- fin si
				end if;

			-- sinon lever ELT_ABSENT
			else
				raise ELT_ABSENT;

			end if;

		-- sinon lever exception ARBRE_VIDE
		else
			raise ARBRE_VIDE;
		end if;

	end Inserer_Parent;

	-- nom : Supprimer_Individu
	-- type : procedure
	-- sémantique : Supprimer un individu donné avec ses ancêtres
	-- pre : /
	-- post : /
	-- exception : ELT_ABSENT, ARBRE_VIDE
	-- TEST : EX : si l'arbre est vide => Supprimer_Individu(arbre,18) => lever exception ARBRE_VIDE
	-- TEST : EX : INDIVIDU : 18 => Supprimer_Individu(arbre,18) => afficher_Arbre(arbre)
	-- TEST : EX : INDIVIDU : 17 n'exsite pas dans l'arbre => Supprimer_Individu(arbre,17) => lever exception ELT_ABSENT
	procedure Supprimer_Individu (Arbre : IN OUT T_ARBRE_GENELOG; Indiv : IN Integer) is

		-- Déclaration de variables
		noeud : T_ARBRE_GENELOG;
	begin
		-- si l'arbre n'est pas vide alors
		if not Est_Vide(Arbre) then

			-- Rechercher l'élément
			noeud := Rechercher(Arbre,Indiv);

			-- si le noeud n'est pas vide alors
			if not Est_Vide(noeud) then

				-- supprimer le noeud
				Supprimer_Noeud(arbre, indiv);

			-- sinon lever ELT_ABSENT
			else
				raise ELT_ABSENT;
			end if;

		-- Sinon lever ARBRE_VIDE
		else
			raise ARBRE_VIDE;

		end if;

	end Supprimer_Individu;

	-- nom : Calculer_Ancetres
	-- type : fonction
	-- type de retour : entier
	-- sémantique : Calculer le nombre d'ancêtres connus d'un T_INDIVIDU (lui compris)
	-- pre :
	-- post :
	-- exception : ELT_ABSENT, ARBRE_VIDE
	-- TEST : EX : si l'arbre est vide => Calcuer_Ancetres(arbre,17) => lever exception ARBRE_VIDE
	-- TEST : EX : INDIVIDU : 18 dans le cas où il a 2 générations connues => Calcuer_Ancetres(arbre,18) => 2
	-- TEST : EX : INDIVIDU : 17 n'exsite pas dans l'arbre => Calcuer_Ancetres(arbre,17) => lever exception ELT_ABSENT
	function Calculer_Ancetres (Arbre : IN T_ARBRE_GENELOG; Indiv : IN Integer) return Integer is

		-- parcourir l'arbre pour calculer le nombre d'ancêtres
		procedure calculer_Nombre (Fa : IN T_ARBRE_GENELOG; N : IN OUT Integer) is

		begin
			-- si le noeud n'est pas vide alors
			if not Est_Vide(Fa) then

				-- incrémenter le noeud
				N := N + 1;

			-- sinon rien
			else
				null;

			end if;

			-- parcourir la partie droite du noeud
			--
			-- le parent1 (Fd) n'est pas vide alors
			if not Est_Vide(Get_Parent1(Fa)) then

				-- appeler la fonction recursive
				calculer_Nombre(Get_Parent1(Fa), N);
			-- sinon
			else
				null;
			end if;

			-- parcourir la partie gauche du noeud
			--
			-- si le parent2 (Fg) n'est pas vide alors
			if not Est_Vide(Get_Parent2(Fa)) then

				-- appeler la fonction récursivement
				calculer_Nombre(Get_Parent2(Fa), N);

			-- sinon
			else
				null;
			end if;

		end calculer_Nombre;

		-- déclaration de variables
		Nombre : Integer := 0; -- le nombre d'ancêtres trouvés
		noeud : T_ARBRE_GENELOG; -- l'individu

	begin
		-- Rechercher l'individu
		noeud := Rechercher(Arbre,Indiv);

		-- si l'individu est trouvé alors
		if not Est_Vide(noeud) then

			-- calculer le nombre de ses ancêtres
			Calculer_Nombre(noeud,Nombre);

			-- retourner le nombre de ses ancêtres
			return Nombre;

		-- sinon lever ELT_ABSENT si l'individu n'est pas trouvé
		else
			raise ELT_ABSENT;
		end if;

	end Calculer_Ancetres;

	-- nom : Obtenir_Ancetres_Generation
	-- type : procedure
	-- sémantique : Obtenir l'ensemble des ancêtres situés à une certaine génération d'un noeud donné
	-- pré : /
	-- post : /
	-- exception : ELT_ABSENT, ARBRE_VIDE
	-- TEST : EX : si l'arbre est vide => Obtenir_Ancetres_Generation(arbre,17) => lever exception ARBRE_VIDE
	-- TEST : EX : INDIVIDU : 18 dans le cas où il a 2 générations connues => Obtenir_Ancetres_Generation(arbre,18) => 4 si tous les parent sont connus
	-- TEST : EX : INDIVIDU : 17 n'exsite pas dans l'arbre => Obtenir_Ancetres_Generation(arbre,17) => lever exception ELT_ABSENT
	procedure Obtenir_Ancetres_Generation (Arbre : IN T_ARBRE_GENELOG; Generation : IN Integer; Indiv : IN Integer) is

		-- parcourir l'arbre pour calculer le nombre d'ancêtres
		procedure calculer_Generation (Fa : IN T_ARBRE_GENELOG; Gen : IN Integer; N : IN Integer := 0) is

		begin
			-- si le counter (N) n'est pas égal à la génération souhaitée alors
			if N /= Gen then

			-- parcourir la partie droite du noeud
			--
			-- le parent1 (Fd) n'est pas null alors
			if not Est_Vide(Get_Parent1(Fa)) then

				-- appeler la fonction recursive
				calculer_Generation(Get_Parent1(Fa), Gen,N+1);
			-- sinon
			else
				null;
			end if;

			-- parcourir la partie gauche du noeud
			--
			-- si le parent2 (Fg) n'est pas vide alors
			if not Est_Vide(Get_Parent2(Fa)) then

				-- appeler la fonction récursivement
				calculer_Generation(Get_Parent2(Fa), Gen,N+1);
			-- sinon
			else
				null;
			end if;

			-- sinon afficher les ancêtres
			else
				outils.Afficher_Noeud(Fa);
			end if;

		end calculer_Generation;

		procedure Afficher_Generation_Trouvees(Fx : IN T_ARBRE_GENELOG; G : IN Integer) is

		begin
			-- la génération fournie doit être positive
			if G >= 0 then

				-- Afficher les ancêtres de la génération donnée
				calculer_Generation(Fx,G);
				new_line;

			-- sinon lever SAISI_INCONNUE
			else
				raise SAISI_INCONNUE;
			end if;

		end Afficher_Generation_Trouvees;

		-- Déclarations de variables
		noeud : T_ARBRE_GENELOG;

	begin
		-- Si l'arbre n'est pas vide
		if not Est_Vide(Arbre) then

			-- Rechercher l'individu
			noeud := Rechercher(Arbre,Indiv);

			-- si l'individu est trouvé alors
			if not Est_Vide(noeud) then

				-- Afficher queleques info
				put("Génération ");
				put(Generation, width => 0);
				put(" de l'individu ");
				put(Indiv, width => 0);
				new_line;

				Afficher_Generation_Trouvees(noeud,Generation);

			-- sinon lever ELT_ABSENT
			else
				raise ELT_ABSENT;
			end if;

		-- sinon lever ARBRE_VIDE
		else
			raise ARBRE_VIDE;
		end if;

	end Obtenir_Ancetres_Generation;

	-- nom : Obtenir_Individu_0Parent
	-- type : procedure
	-- sémantique : Obtenir l'ensemble des Individus qui n'ont aucun parent connu
	-- pré : /
	-- post : /
	-- exception : ARBRE_VIDE
	-- TEST : => Obtenir_Individu_0Parent(arbre) => Affiche (comme une liste) les individus (comme une liste) qui n'ont aucun parent connu
	-- TEST : si l'larbre est vide => Obtenir_Individus_0Parent(arbre) => lever exception ARBRE_VIDE
	procedure Obtenir_Individus_0Parent (Arbre : IN T_ARBRE_GENELOG) is

	begin
		-- si l'arbre n'est pas vide alors
		if not Est_Vide(Arbre) then

			-- chercher les ancêtres
			outils.Chercher_Ensemble_Parent(Arbre,0);
		-- sinon lever ARBRE_VIDE
		else
			raise ARBRE_VIDE;

		end if;

	end Obtenir_Individus_0Parent;

	-- nom : Obtenir_Individu_1Parent
	-- type : procedure
	-- sémantique : Obtenir l'ensemble des Individus qui n'ont qu'un parent connu
	-- pré : /
	-- post : /
	-- exception : ARBRE_VIDE
	-- TEST : => Obtenir_Individus_1Parent(arbre) => Affiche (comme une liste) les individus (comme une liste) qui n'ont aucun parent connu
	-- TEST : si l'larbre est vide => Obtenir_Individus_1Parent(arbre) => lever exception ARBRE_VIDE
	procedure Obtenir_Individus_1Parent (Arbre : IN T_ARBRE_GENELOG) is

	begin
		-- si l'arbre n'est pas vide
		if not Est_Vide(Arbre) then

			-- chercher les ancêtres
			outils.Chercher_Ensemble_Parent(Arbre,1);

		-- sinon lever ARBRE_VIDE
		else
			raise ARBRE_VIDE;

		end if;

	end Obtenir_Individus_1Parent;

	-- nom : Obtenir_Individu_2Parent
	-- type : procedure
	-- sémantique : Obtenir l'ensemble des Individus qui ont deux parents connus
	-- pré : /
	-- post : /
	-- exception : ARBRE_VIDE
	-- TEST : => Obtenir_Individus_2Parent(arbre) => Affiche (comme une liste) les individus (comme une liste) qui n'ont aucun parent connu
	-- TEST : si l'arbre est vide => Obtenir_Individus_2Parent(arbre) => lever exception ARBRE_VIDE
	procedure Obtenir_Individus_2Parent(Arbre : IN T_ARBRE_GENELOG) is

	begin
		-- si l'arbre n'est pas vide
		if not Est_Vide(Arbre) then

			-- chercher les ancêtres
			outils.Chercher_Ensemble_Parent(Arbre,2);

		-- sinon lever ARBRE_VIDE
		else
			raise ARBRE_VIDE;

		end if;

	end Obtenir_Individus_2Parent;

	-- nom : Afficher_Arbre
	-- type : procedure
	-- sémantique : Afficher un arbre à partir d'un noeud donné
	-- pré : /
	-- post : /
	-- exception : /
	-- test : affichage de l'arbre
	procedure Afficher_Arbre (Arbre : IN T_ARBRE_GENELOG; id : IN Integer) is

		noeud : T_ARBRE_GENELOG;

		-- parcourir l'arbre pour afficher noeud par noeud
		procedure Parcours_Affichage (Fa : IN T_ARBRE_GENELOG; espaces : IN Integer := 0) is

		begin
			if not Est_Vide(Fa) then

				-- Afficher la génération
				put("Génération : ");
				put(espaces, width => 0);
				new_line;
				put_line("-----------------");

				outils.Afficher_Noeud_Avec_Espaces(Fa,espaces);

				-- affichage préfixé
				Parcours_Affichage(Get_Parent1(Fa), espaces+1);
				Parcours_Affichage(Get_Parent2(Fa), espaces+1);
			else
				null;
			end if;

		end Parcours_Affichage;


	begin
		-- Si l'arbre n'est pas vide
		if not Est_Vide(Arbre) then

			-- Rechercher l'individu
			noeud := Rechercher(Arbre,id);

			-- si l'individu est trouvé alors
			if not Est_Vide(noeud) then

				-- Afficher l'arbre
				Parcours_Affichage(noeud);

			-- sinon lever ELT_ABSENT
			else
				raise ELT_ABSENT;
			end if;

		-- sinon lever ARBRE_VIDE
		else
			raise ARBRE_VIDE;
		end if;

	end Afficher_Arbre;

end p_arbre_genelog;
