with p_arbre_genelog; use p_arbre_genelog;
with p_individu; use p_individu;
with ada.assertions; use ada.assertions;
with text_io; use text_io;
with ada.Integer_Text_Io; use ada.Integer_Text_Io;


procedure test_arbre_genelog is

	-- Déclaration des variables
	arbre : T_ARBRE_GENELOG;
	racine : T_Individu;
	individu : T_INDIVIDU;
	ancetres : Integer := 0;
begin

	--------------------------------------------------------------------------------------------------------------
	
	-- creer une racine
	racine := Creer_Individu(Creer_Nouveau_Id,("toto1111111111111111",4),("Titi1111111111111111",4),(11,20,2003),'1');
	
	-- initialiser l'arbre avec la creation d'une racine de type T_Individu
	Creer_Arbre_Genelog(arbre,racine);
	
	assert(not Est_Arbre_Vide(arbre), "ERREUR : L'arbre généalogique est encore vide après l'Initialisation");
	
	---------------------------------------------------------------------------------------------------------------
	
	-- Créer un individu
	individu := Creer_Individu(Creer_Nouveau_Id,("11111111111111111111",4),("11111111111111111111",4),(30,12,2000),'1');

	-- insérer un parent pour la racine
	Inserer_Parent(arbre,1,individu);
	
	---------------------------------
	individu := Creer_Individu(Creer_Nouveau_Id,("22221111111111111111",4),("22221111111111111111",4),(30,12,2000),'2');

	-- insérer un autre parent pour 1
	Inserer_Parent(arbre,1,individu);
	

	---------------------------------

	individu := Creer_Individu(Creer_Nouveau_Id,("22221111111111111111",4),("22221111111111111111",4),(30,12,2000),'2');

	-- insérer un parent pour 3
	Inserer_Parent(arbre,3,individu);

	---------------------------------

	individu := Creer_Individu(Creer_Nouveau_Id,("33331111111111111111",4),("33331111111111111111",4),(30,12,2000),'1');
	
	-- insérer un autre parent pour 3
	Inserer_Parent(arbre,3,individu);
	
	---------------------------------
	individu := Creer_Individu(Creer_Nouveau_Id,("22221111111111111111",4),("22221111111111111111",4),(30,12,2000),'2');

	-- insérer un autre parent pour 1
	Inserer_Parent(arbre,4,individu);
	---------------------------------

	individu := Creer_Individu(Creer_Nouveau_Id,("33331111111111111111",4),("33331111111111111111",4),(30,12,2000),'1');
	
	-- insérer un parent pour 2
	Inserer_Parent(arbre,2,individu);
	---------------------------------

		
	-- insérer un parent pour un individu qui n'existe pas
	begin  
		individu := Creer_Individu(Creer_Nouveau_Id,("33331111111111111111",4),("33331111111111111111",4),(30,12,2000),'1');
	
		Inserer_Parent(arbre,9,individu);

	exception 
		when ELT_ABSENT => put_line("ELT ABSENT : Inserer_Parent(arbre,9,individu)");
	end;
	------------------------------------------------------------------------------------------------------------------
	
	-- L'affichage doit correspondre à la bonne insertion effectuée précédement
	Afficher_Arbre(Arbre, 1);

	put_line("--------------------------------------------------------------------");

	------------------------------------------------------------------------------------------------------------------
	
	-- Calculer les ancêtres d'un individu
	--
	
	-- TEST : EX : INDIVIDU : 18 dans le cas où il a 2 générations connues => Calcuer_Ancetres(arbre,18) => 2 
	
	new_line;
	put("Le nombre d'ancêtres de l'individu 1 sont : ");
	put(Calculer_Ancetres(arbre,1));

	-- TEST : EX : INDIVIDU : 17 n'exsite pas dans l'arbre => Obtenir_Ancetres_Generation(arbre,17) => lever exception ELT_ABSENT
	new_line;
	begin 
		ancetres := Calculer_Ancetres(arbre,17);
		put(ancetres);

	exception
		when ELT_ABSENT => put_line("ELT_ABSENT : Calculer_Ancetres(arbre,17)");
	end;
	------------------------------------------------------------------------------------------------------------------
	
	-- Obtenir les ancetres d'une generation
	--
	-- TEST : EX : si l'arbre est vide => Calcuer_Ancetres(arbre,17) => lever exception ARBRE_VIDE


	-- TEST : EX : INDIVIDU : 1 dans le cas où il a 2 générations connues => Obtenir_Ancetres_Generation(arbre,2,1) afficher l'arbre jusqu'à la génération demandée
	Obtenir_Ancetres_Generation(arbre,2,1); 
	
	-- TEST : EX : INDIVIDU : 17 n'exsite pas dans l'arbre => Obtenir_Ancetres_Generation(arbre,17) => lever exception ELT_ABSENT
	begin 
		Obtenir_Ancetres_Generation(arbre,2,17);

	exception
		when ELT_ABSENT => put_line("ELT_ABSENT : Obtenir_Ancetres_Generation(arbre,2,17)");
	end; 

	-------------------------------------------------------------------------------------------------------------------

	-- Lister les individus qui n'ont aucun parent connue
	-- TEST : => Obtenir_Individus_0Parent(arbre) => Affiche (comme une liste) les individus (comme une liste) qui n'ont aucun parent connu
	new_line;
	put_line("Les individus qui n'ont aucun parent connue");
	
	Obtenir_Individus_0Parent(arbre);

	-------------------------------------------------------------------------------------------------------------------
	
	-- Lister les individus qui n'ont qu'un seul parent connue
	-- TEST : => Obtenir_Individus_1Parent(arbre) => Affiche (comme une liste) les individus (comme une liste) qui n'ont aucun parent connu
	new_line;
	put_line("Les individus qui n'ont qu'un seul parent connue"); 
	Obtenir_Individus_1Parent(arbre); 
	
	--------------------------------------------------------------------------------------------------------------------
	
	-- Lister les individus qui ont 2 parents connues
	-- TEST : => Obtenir_Individus_2Parent(arbre) => Affiche (comme une liste) les individus (comme une liste) qui n'ont aucun parent connu
	new_line;
	put_line("Les individus qui ont 2 parents connues");  
	Obtenir_Individus_2Parent(arbre);
	
	----------------------------------------------------------------------------------------------------------------------
	
	-- Supprimer un individu
	-- TEST : EX : INDIVIDU : 17 n'exsite pas dans l'arbre => Supprimer_Individu(arbre,17) => lever exception ELT_ABSENT
	begin 
		Supprimer_Individu(arbre,17);
	exception
		when ELT_ABSENT => put_line("ELT_ABSENT : Supprimer_Individu(arbre,17)");
	end;
	
	-- TEST : EX : INDIVIDU : 1 => Supprimer_Individu(arbre,1) => Est_Vide(Rechercher(arbre,1)) au lieu de rechercher on affiche directement l'arbre
	Supprimer_Individu(arbre,1);

	----------------------------------------------------------------------------------------------------------------------
	new_line;
	put_line("---------------------- maintenant l'arbre est vide --------------------------");

	-- afficher l'arbre => une exception ARBRE_VIDE doit être levée
	begin
		Afficher_Arbre(arbre, 1);
	exception
		when ARBRE_VIDE => put_line("ARBRE VIDE : Afficher_Arbre(arbre, 1)");
	end;

	-- TEST : EX : si l'arbre est vide => Supprimer_Individu(arbre,18) => lever exception ARBRE_VIDE
	begin 
		Supprimer_Individu(arbre,17);
	exception
		when ARBRE_VIDE => put_line("ARBRE_VIDE : Supprimer_Individu(arbre,17)");
	end;
	
	-- TEST : si l'larbre est vide => Obtenir_Individus_0Parent(arbre) => lever exception ARBRE_VIDE 
	begin 
		Obtenir_Individus_0Parent(arbre);

	exception
		when ARBRE_VIDE => put_line("ARBRE_VIDE : Obtenir_Individus_0Parent(arbre) ");
	end;

	---------------------
	-- TEST : si l'larbre est vide => Obtenir_Individus_1Parent(arbre) => lever exception ARBRE_VIDE 
	begin 
		Obtenir_Individus_1Parent(arbre);

	exception
		when ARBRE_VIDE => put_line("ARBRE_VIDE : Obtenir_Individus_1Parent(arbre)");
	end;
	---------------------
	-- TEST : si l'larbre est vide => Obtenir_Individus_2Parent(arbre) => lever exception ARBRE_VIDE 
	begin 
		Obtenir_Individus_2Parent(arbre);

	exception
		when ARBRE_VIDE => put_line("ARBRE_VIDE : Obtenir_Individus_2Parent(arbre)");
	end;
	---------------------
	-- TEST : si l'larbre est vide => Obtenir_Ancetres_Generation(arbre,2,17); => lever exception ARBRE_VIDE 
	begin 
		Obtenir_Ancetres_Generation(arbre,2,17);

	exception
		when ARBRE_VIDE => put_line("ELT_ABSENT : Obtenir_Ancetres_Generation(arbre,2,17)");
	end; 


--exception
--	when others => put_line("ERREUR INCONNUE : Probablement le corps des programmes n'est pas encore défini");
	
end;
