-- Le package p_arbre_bin_generig V3

generic

	-- déclaration de type
	type T_ELEMENT is private;  -- le type d'element qu'on veut avoir dans l'arbre

	type T_ID is private; -- Le type avec le quel l'egalité dans la fonction Egal_id est faite
			      -- cela permet de contrôler le type d'id qu'un autre package veut utiliser

	with function Egal_id ( parent : IN T_ELEMENT; un_id : IN T_ID ) return boolean; -- comparer en fonction d'un id

	with function Egal_Elt ( parent1, parent2 : IN T_ELEMENT) return boolean; -- Comparer Deux éléments dans l'arbre

	-- Les changements des specs pour cette version
	-- Les post et pré condition de la procédure Supperimer_Parent
	-- La fonction Rechercher_Avec_Noeud est supprimée
	--

-- Ce package suit la programmation offensive

package p_arbre_bin_generig is

	-- Déclaration de types privés
	type T_ARBRE_BIN is private;

	-- nom : Est_Vide
	-- type : fonction
	-- type de retour : boolean
	-- sémantique : Voir si l'arbre est vide ou pas
	-- pre : /
	-- post : /
	-- exception : /
	-- TEST : arbre vide => EST_Vide(arbre) = vrai, faux sinon
	function Est_Vide (arbre : IN T_ARBRE_BIN) return boolean;

	-- nom : Init
	-- type : fonction
	-- type de retour : T_ARBRE_BIN
	-- sémantique : Initialiser un arabre binaire
	-- pre : /
	-- post : Est_vide(arbre)
	-- exception : /
	-- TEST : Est_vide(arbre) = vrai
	function Init return T_ARBRE_BIN with
		Post => Est_Vide(Init'Result);

	-- nom : Ajouter_Racine
	-- type : procedure
	-- sémantique : Créer la racine d'un arbre
	-- pre : Est_Vide(Fa)
	-- post : non Est_vide(Fa) et alors Egal_Elt(Get_Noeud_Elt(Fa), F_ELEMENT)
	-- exception : /
	-- TEST : racine : 0 => Ajouter_Racine(arbre,racine) => L'arbre a une racine dans ce cas là et n'est plus vide
	procedure Ajouter_Racine (Fa : IN OUT T_ARBRE_BIN; F_ELEMENT : IN T_ELEMENT) with
		Pre => Est_vide(Fa),
		Post => not Est_vide(Fa) and then Egal_Elt(Get_Noeud_Elt(Fa), F_ELEMENT);

	-- nom : Ajouter_Parent1
	-- type : procedure
	-- sémantique : Ajouter le parent1 (noeud/Fd) à un noeud
	-- pre :  non Est_Vide(Fa) et alors Est_Vide(Get_Parent1(Fa))
	-- post :  non Est_Vide(Fa) et non Est_Vide(Get_Parent1(Fa))
	--       	   et alors Egal_Elt(Get_Noeud_Elt(Get_Parent1(Fa)), F_ELEMENT)
	-- exception : /
	-- TEST : Inserer 18 à 20 en tant que parent1 => 20 a un parent1 qui est 18
	procedure Ajouter_Parent1 (Fa : IN OUT T_ARBRE_BIN; F_ELEMENT : IN T_ELEMENT) with
		pre => not Est_vide(Fa) and then Est_Vide(Get_Parent1(Fa)),
	       post => (not Est_Vide(Fa) and not Est_Vide(Get_Parent1(Fa)))
	       	       and then Egal_Elt(Get_Noeud_Elt(Get_Parent1(Fa)), F_ELEMENT);

	-- nom : Ajouter_Parent2
	-- type : procedure
	-- sémantique : Ajouter le parent2 (noeud/Fg) à un noeud
	-- pre :  non Est_Vide(Fa) et alors Est_Vide(Get_Parent2(Fa))
	-- post :  non Est_Vide(Fa) et non Est_Vide(Get_Parent2(Fa))
	--       	   et alors Egal_Elt(Get_Noeud_Elt(Get_Parent2(Fa)), F_ELEMENT)
	-- exception : /
	-- TEST : Inserer 18 à 20 en tant que parent2 => 20 a un parent2 qui est 18
	procedure Ajouter_Parent2 (Fa : IN OUT T_ARBRE_BIN; F_ELEMENT : IN T_ELEMENT) with
		pre => not Est_vide(Fa) and then Est_Vide(Get_Parent2(Fa)),
	       post => (not Est_Vide(Fa) and not Est_Vide(Get_Parent2(Fa)))
	       	       and then Egal_Elt(Get_Noeud_Elt(Get_Parent2(Fa)), F_ELEMENT);


	-- nom : Supprimer_Parent
	-- type : procedure
	-- type de retour : T_ARBRE_BIN
	-- sémantique : supprimer un parent et tous ses sous-noeuds d'un noeud donné
	-- pre : /
	-- post :  Est_Vide(Rechercher(Fa,Fe));
	-- exception : /
	-- TEST : EX : noeud : 18 parent1 : 19 => Supprimer_Parent (noeud,19) => parent1 = null
	procedure Supprimer_Noeud (Fa : IN OUT T_ARBRE_BIN; Fe : IN T_ID) with
		Post => Est_Vide(Rechercher(Fa,Fe)); -- // Changement de spec

	-- nom : Rechercher
	-- type : function
	-- type de retour : T_ARBRE_BIN
	-- sémantique : Rechercher un noeud dans l'arbre
	-- pre : /
	-- post : /
	-- exception : /
	-- TEST : noeud : 18 => Rechercher(arbre,18) => return arbre = 18
	function Rechercher (Fa : IN T_ARBRE_BIN; Fe : IN T_ID) return T_ARBRE_BIN;

	-- nom : Get_Noeud_Elt
	-- type : function
	-- sémantique : Retourner l'élément d'un noeud
	-- type de retour : T_ELEMENT
	-- pre : non Est_Vide(Fa)
	-- post : /
	-- exception : /
	-- TEST : noeud = 18 => Get_Noeud_Elt(noeud) => 18
	function Get_Noeud_Elt (Fa : IN T_ARBRE_BIN) return T_ELEMENT with
		pre => not Est_vide(Fa); -- Changement de spec

	-- nom : Get_Parent1
	-- type : function
	-- sémantique : Retourner le parent1 (Fils droite) d'un noeud donné
	-- type de retour : T_ARBRE_BIN
	-- pre : /
	-- post : /
	-- exception : /
	-- TEST : noeud : 18 , parent1 : 19 => Get_Parent1(noeud) => noeud = 19
        function Get_Parent1 (Fa : IN T_ARBRE_BIN) return T_ARBRE_BIN;

	-- nom : Get_Parent2
	-- type : function
	-- sémantique : Retourner le parent2 (Fils gauche) d'un noeud donné
	-- type de retour : T_ARBRE_BIN
	-- pre : /
	-- post : /
	-- exception : /
	-- TEST : noeud : 18 , parent2 : 19 => Get_Parent2(noeud) => noeud = 19
	function Get_Parent2 (Fa : IN T_ARBRE_BIN) return T_ARBRE_BIN;

	private

	-- Déclaration de types

	-- le noeud d'un arbre
	type T_NOEUD;

	-- l'arbre lui-même (est un pointeur sur T_NOEUD)
	type T_ARBRE_BIN is access T_NOEUD;

	type T_NOEUD is record
		Elt : T_ELEMENT; -- l'élément de l'arbre
		parent1 : T_ARBRE_BIN; -- le parent1 (Fils droite) d'un noeud
		parent2 : T_ARBRE_BIN; -- la parent2 (Fils gauche) d'un noeud
	end record;
end;
